//
//  ContentView.swift
//  Pokedex
//
//  Created by Jorge Bobrek on 25/05/22.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var manager = NetworkingManager()
    @Environment(\.horizontalSizeClass) var sizeClass
    
    var body: some View {
        ScrollView {
            VStack(spacing: 0) {
                HStack {
                    if sizeClass == .compact{
                        Spacer()
                    } else {
                        Spacer()
                            .frame(width: 10)
                    }
                    Image("Logo")
                        .resizable()
                        .frame(width: 32, height: 32)
                    Text("POKÉDEX_")
                        .font(.headline.bold())
                        .foregroundColor(.white)
                    Spacer()
                }
                .padding(.vertical, 10)
                .background(Color("Gray39"))
                .cornerRadius(20, corners: [.topLeft, .topRight])
                
                if sizeClass == .compact{
                    VStack(spacing: 0) {
                        PokemonData(self.manager.pokemon, sizeClass)
                    }
                    .background(Color("Gray68"))
                } else {
                    HStack(spacing: 30) {
                        PokemonData(self.manager.pokemon, sizeClass)
                    }
                    .background(Color("Gray68"))
                }
                
                VStack {
                    if sizeClass == .compact{
                        Text("OTHERS")
                            .font(.title2)
                        HStack(spacing: 20) {
                            Spacer()
                            ButtonOther(self.manager.others[0].image) {
                                self.manager.displayPokemon(pokemon: self.manager.others[0])
                            }
                            Spacer()
                            ButtonOther(self.manager.others[1].image) {
                                self.manager.displayPokemon(pokemon: self.manager.others[1])
                            }
                            Spacer()
                        }
                        HStack(spacing: 20) {
                            Spacer()
                            ButtonOther(self.manager.others[2].image) {
                                self.manager.displayPokemon(pokemon: self.manager.others[2])
                            }
                            Spacer()
                            ButtonOther(self.manager.others[3].image) {
                                self.manager.displayPokemon(pokemon: self.manager.others[3])
                            }
                            Spacer()
                        }
                    } else {
                        HStack{
                            Spacer()
                            Text("OTHERS")
                                .font(.title2)
                            ForEach(self.manager.others, id: \.id) { pokemon in
                                Spacer()
                                ButtonOther(pokemon.image) {
                                    self.manager.displayPokemon(pokemon: pokemon)
                                }
                            }
                            Spacer()
                        }
                    }
                }
                .padding(20)
                .background(.white)
                .cornerRadius(20, corners: [.bottomLeft, .bottomRight])
            }
            .overlay(RoundedRectangle(cornerRadius: 20).stroke(Color("Gray39")))
            .padding()
        }
        .background(Color("Gray72"))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewInterfaceOrientation(.portrait)
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

struct RoundedCorner: Shape {
    
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

struct ImageMain: View {
    var name: String
    
    init(_ name: String){
        self.name = name
    }
    
    var body: some View{
        AsyncImage(url: URL(string: name)){ phase in
            switch phase {
            case .empty:
                ProgressView()
                    .scaleEffect(3)
            case .success(let image):
                image
                    .resizable()
            case .failure:
                Image("MissingNo")
                    .resizable()
            @unknown default:
                Image("MissingNo")
                    .resizable()
            }
        }
        .frame(width: 300, height: 300)
    }
}

struct ButtonOther: View {
    var name: String
    var action: () -> Void
    
    init(_ name: String,_ action: @escaping () -> Void){
        self.name = name
        self.action = action
    }
    
    var body: some View {
        Button(action: action){
            AsyncImage(url: URL(string: name)){ phase in
                switch phase {
                case .empty:
                    ProgressView()
                        .scaleEffect(3)
                case .success(let image):
                    image
                        .resizable()
                case .failure:
                    Image("MissingNo")
                        .resizable()
                @unknown default:
                    Image("MissingNo")
                        .resizable()
                }
            }
            .frame(width: 92, height: 92)
            .colorMultiply(.black)
            .background(RoundedRectangle(cornerRadius: 10)
                .foregroundColor(Color("Gray68")))
        }
    }
}

struct Types: View {
    var types: [String]
    var image: Bool
    
    init(_ types: [String], image: Bool = true) {
        self.types = types
        self.image = image
    }
    
    var body: some View{
        HStack(spacing:0){
            if image {
                TypeImage(types[0])
                if types.count > 1 {
                    TypeImage(types[1])
                        .padding(.leading, 5)
                }
            } else {
                Text(types[0].uppercased())
                if types.count > 1 {
                    Text("/" + types[1].uppercased())
                }
            }
        }
    }
}

struct TypeImage: View {
    var type: String
    
    init(_ type: String) {
        self.type = type
    }
    
    var body: some View{
        if UIImage(named: type.firstUppercased) != nil {
            Image(type.firstUppercased)
                .resizable()
                .frame(width: 32, height: 32)
        } else {
            Text(type.uppercased())
        }
    }
}

extension String {
    var firstUppercased: String { prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { prefix(1).capitalized + dropFirst() }
}

struct PokemonData: View {
    let pokemon: Pokemon
    let sizeClass: UserInterfaceSizeClass?
    
    init (_ pokemon: Pokemon,_ sizeClass: UserInterfaceSizeClass?){
        self.pokemon = pokemon
        self.sizeClass = sizeClass
    }
    
    var body: some View {
        Group {
            Spacer()
            VStack {
                HStack {
                    Spacer()
                    Types(pokemon.types)
                    Text(pokemon.name.uppercased())
                        .font(.title.bold())
                    Spacer()
                }
                .padding(.vertical, 10)
                .frame(
                    width: 300
                )
                .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.white))
                .padding(.top, 20)
                
                ImageMain(pokemon.image)
            }
            
            if sizeClass == .compact {
                HStack {
                    Spacer()
                    VStack(alignment: .leading, spacing: 20) {
                        Text("NO.")
                        Text("LEVEL")
                        Text("TYPE")
                        Text("ABILITY")
                        Text("HEIGHT")
                        Text("WEIGHT")
                    }
                    .padding(.vertical, 20)
                    Spacer()
                    VStack(alignment: .leading, spacing: 20) {
                        Text(String(format: "%03d", pokemon.id))
                        Text("100")
                        Types(pokemon.types, image: false)
                            .frame(height: 20)
                        Text(pokemon.ability.uppercased())
                        Text(String(format: "%.1f m", Float(pokemon.height)/10))
                        Text(String(format: "%.1f Kg", Float(pokemon.weight)/10))
                    }
                    .padding(.vertical, 20)
                    Spacer()
                }
                .padding(.vertical, 10)
                .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.white))
                .padding(EdgeInsets(top: 0, leading: 30, bottom: 20, trailing: 30))
            } else {
                VStack {
                    Spacer()
                    HStack {
                        VStack {
                            VStack {
                                Text("NO.")
                                Text(String(format: "%03d", pokemon.id))
                            }
                            .padding()
                            VStack {
                                Text("TYPE")
                                Types(pokemon.types, image: false)
                                    .frame(height: 20)
                            }
                            .padding()
                            VStack {
                                Text("HEIGHT")
                                Text(String(format: "%.1f m", Float(pokemon.height)/10))
                            }
                            .padding()
                        }
                        VStack {
                            VStack {
                                Text("LEVEL")
                                Text("100")
                            }
                            .padding()
                            VStack {
                                Text("ABILITY")
                                Text(pokemon.ability.uppercased())
                            }
                            .padding()
                            VStack {
                                Text("WEIGHT")
                                Text(String(format: "%.1f Kg", Float(pokemon.weight)/10))
                            }
                            .padding()
                        }
                    }
                    .padding()
                    .frame(
                        maxHeight: .infinity
                    )
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.white))
                    .padding(.vertical, 15)
                    Spacer()
                }
            }
            Spacer()
        }
        .background(Color("Gray68"))
    }
}
