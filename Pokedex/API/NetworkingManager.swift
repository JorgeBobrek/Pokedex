//
//  NetworkingManager.swift
//  Pokedex
//
//  Created by Jorge Bobrek on 25/05/22.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkingManager: ObservableObject {
    
    @Published var pokemon = Pokemon(id: 0, types: ["bird"], ability: "???", height: 10, weight: 100, name: "MissingNo", image: "")
    
    @Published var others = [
        Pokemon(id: 0, types: ["bird"], ability: "???", height: 10, weight: 100, name: "MissingNo", image: ""),
        Pokemon(id: 0, types: ["bird"], ability: "???", height: 10, weight: 100, name: "MissingNo", image: ""),
        Pokemon(id: 0, types: ["bird"], ability: "???", height: 10, weight: 100, name: "MissingNo", image: ""),
        Pokemon(id: 0, types: ["bird"], ability: "???", height: 10, weight: 100, name: "MissingNo", image: "")
    ]
    
    init() {
        self.loadPokemon(id: Int.random(in: 1...898))
        self.refresh()
    }
    
    func refresh() {
        for index in 0...3 {
            DispatchQueue.main.async {
                AF.request("https://pokeapi.co/api/v2/pokemon/\(Int.random(in: 1...898))").responseJSON { [self] (response) in
                    switch response.result {
                    case .success(let value):
                        self.others[index] = self.savePokemon(json: JSON(value))
                    case .failure(let error):
                        print(error)
                        self.pokemon = Pokemon(id: 0, types: ["bird"], ability: "???", height: 10, weight: 100, name: "MissingNo", image: "")
                    }
                }
            }
        }
    }
    
    func displayPokemon(pokemon: Pokemon){
        self.pokemon = pokemon
        self.refresh()
    }
    
    func loadPokemon(id: Int) {
        DispatchQueue.main.async {
            AF.request("https://pokeapi.co/api/v2/pokemon/\(String(id))").responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    self.pokemon = self.savePokemon(json: JSON(value))
                case .failure(let error):
                    print(error)
                    self.pokemon = Pokemon(id: 0, types: ["bird"], ability: "???", height: 10, weight: 100, name: "MissingNo", image: "")
                }
            }
        }
    }
    
    func savePokemon(json: JSON) -> Pokemon {
        var types = [String]()
        for item in json["types"] {
            types.append(item.1["type"]["name"].string ?? "")
        }
        return Pokemon(id: json["id"].int ?? 0,
                          types: types,
                          ability: json["abilities"][0]["ability"]["name"].string ?? "",
                          height: json["height"].int ?? 0,
                          weight: json["weight"].int ?? 0,
                          name: json["species"]["name"].string ?? "",
                          image: json["sprites"]["other"]["official-artwork"]["front_default"].string ?? "")
    }
    
}
