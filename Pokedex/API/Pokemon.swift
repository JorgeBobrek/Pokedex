//
//  Pokemon.swift
//  Pokedex
//
//  Created by Jorge Bobrek on 25/05/22.
//

import Foundation

struct Pokemon: Identifiable {
    var id: Int
    var types: [String]
    var ability: String
    var height: Int
    var weight: Int
    var name: String
    var image: String
}

