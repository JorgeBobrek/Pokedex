//
//  PokedexApp.swift
//  Pokedex
//
//  Created by Jorge Bobrek on 25/05/22.
//

import SwiftUI

@main
struct PokedexApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
